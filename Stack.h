#ifndef STACK_H
#define STACK_H

#include <cassert>
#include <iostream>
#include <iomanip>

using namespace std;

template <typename Stack_>
class Stack
{
private:
	Stack_* StackPtr;
	const int Size;
	int Top;
public:
	Stack(int = 10);
	~Stack();

	void push(const Stack_&);
	void pop();
	void printStack();
};

template <typename Stack_>
Stack<Stack_>::Stack(int maxSize) : Size(maxSize)
{
	StackPtr = new Stack_[Size];
	Top = 0;
}

template <typename Stack_>
Stack<Stack_>::~Stack()
{
	delete[] StackPtr;
}

template <typename Stack_>
void Stack<Stack_>::push(const Stack_& value)
{
	assert(Top < Size);

	StackPtr[Top++] = value;
}

template <typename Stack_>
void Stack<Stack_>::pop()
{
	assert(Top > 0);

	StackPtr[--Top];
}

template <typename Stack_>
void Stack<Stack_>::printStack()
{
	for (int i = Top - 1; i >= 0; i--)
		cout << "|" << setw(4) << StackPtr[i] << endl;
}

#endif // STACK_H
