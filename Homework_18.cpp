﻿#include <iostream>
#include "Stack.h"

using namespace std;

int main()
{
    setlocale(0, "");

    Stack<char> StackSymbol(7);
    int ct = 0;
    char ch;

    while (ct++ < 7)
    {
        cin >> ch;
        StackSymbol.push(ch);
    }

    cout << "\n\nЭлементы стека\n";
    StackSymbol.printStack();

    cout << "\n\nУдалим элемент из стека\n";
    StackSymbol.pop();

    StackSymbol.printStack();
}
